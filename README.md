# README #

A demo app that used to normalise pre-set phone numbers, detect if the number is a mobile number, fixed line and whether it's a valid number for the country where the mobile device located. 

It demonstrates the use of BDD, MVP, Code style enforcement etc.

### How do I get set up? ###

open the workspace file and build

### Contribution guidelines ###

Please read

PhoneNumbersNormaliser.pdf - the details of development approach used on this project.

### Who do I talk to? ###

hikaru.ip@gmail.com